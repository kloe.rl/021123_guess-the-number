window.addEventListener('DOMContentLoaded', function(event) {

// Conservation du score avec LocalStorage
    // Déclaration variable tries qui contient le nombre de tentative pour le nombre à deviner en cours
    let score = 0;
    // Récupération de <p> bestTries qui contiendra le meilleur score à restaurer
    const bestTries = document.querySelector('#bestTries'); 
    // Condition qui vérifie si bestScoreUser existe
    if (!localStorage.getItem('bestScoreUser')) {
        bestTries.textContent = "Vous n'avez pas encore de meilleur score enregistré !"
    } else {
        bestTries.textContent = "Votre meilleur score est de " + localStorage.getItem('bestScoreUser');
    };

    // Création fonction setBestScoreUser()
    function setBestScoreUser(score) {
        localStorage.setItem('bestScoreUser', score);
        bestTries.textContent = "Votre meilleur score est de " + score;
    };

    // Déclaration valeur secrète entre 0 et 99
    let randomNumber;
    // Récupération du formulaire
    const form = document.getElementById("guess_form");
    // Récupération de <p> .guessingIndicator qui contiendra les messages
    const indicator = document.querySelector(".guessingIndicator");
    // Récupération de <p> .lastValue qui contiendra le dernier nombre saisi par le joueur
    const currentValue = document.querySelector(".lastValue");
    // Récupération de <p> .nbTry qui affiche le nombre de tentative de la session actuelle
    const attempts = document.querySelector(".nbTries");
    // Récupération de <p> .score qui contiendra le score du joueur
    // const currentScore = document.querySelector(".score");

    // Fonction startGame
	function startGame(){
		score = 0;
		randomNumber = Math.floor(Math.random() * 100);

		// Je rajoute ici un console.log() qui permet d'afficher le nombre qui a été tiré au hasard
		console.log('Le nombre à deviner est : ' + randomNumber);
    };

    //Lancement du jeu
	startGame();

    // Fonction submitForm
    function submitForm(event) {
        event.preventDefault();
        score ++
        // Déclaration de value (nombre saisi par le joueur)
        const value = form.guess.value;
        let message = "";
        if (value > randomNumber) {
            message = "Plus petit !";
            document.getElementById("guess_form").reset();
            // Insère le contenu de value dans HTML <p> .lastValue
            currentValue.textContent = "Votre nombre : " + value;            
        } else if (value < randomNumber) {
            message = "Plus grand !";
            document.getElementById("guess_form").reset();
            // Insère le contenu de value dans HTML <p> .lastValue
            currentValue.textContent = "Votre nombre : " + value;
        } else {
            message = "C'est gagné !";
            // score ++;
            let currentBestScore = localStorage.getItem('bestScoreUser');
            console.log(currentBestScore == "undefined", typeof currentBestScore)
            if (currentBestScore === "undefined" || currentBestScore === null ||currentBestScore > score) {
                setBestScoreUser(score);
            };
            //Efface le contenu du formulaire
            document.getElementById("guess_form").reset();
            currentValue.textContent = "Le nombre est " + value + " !";
            startGame();
        }
        // Insère le contenu de message dans HTML <p> .guessingIndicator
        indicator.textContent = message;
        // Insère le contenu de tries dans HTML <p> .nbTries
        attempts.textContent = "Nombre de tentatives : " + score;
    };
    
    // Au clic, lance la fonction submitForm
    form.addEventListener('submit', submitForm);
});  

